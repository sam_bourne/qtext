..
    :copyright: Copyright (c) 2014 ftrack

..

*************
Release Notes
*************

.. release:: 0.2.0

    .. change:: add

        Provide QtExt.QtWebCompat module for limited compatiblity purposes to QWebPage and QWebKit.